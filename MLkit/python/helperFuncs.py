import ROOT
from math import *
import random


def px(obj):
  return obj['pt'] * cos(obj['phi'])


def py(obj):
  return obj['pt'] * sin(obj['phi'])


def pz(obj):
  return obj['pt'] * sinh(obj['eta'])


def psquare(x, y, z):
  return (x * x) + (y * y) + (z * z)


def deltaPhi(phi1, phi2):
  dphi = phi2-phi1
  if  dphi > pi:
    dphi -= 2.0*pi
  if dphi <= -pi:
    dphi += 2.0*pi
  return abs(dphi)


def deltaR2(l1, l2):
  return deltaPhi(l1['phi'], l2['phi'])**2 + (l1['eta'] - l2['eta'])**2


def deltaR(l1, l2):
  return sqrt(deltaR2(l1,l2))


def getYieldFromChain(c, cutString = "(1)", lumi = "10000.", weight = "weight * xs_weight * sf_total * weight_sherpa22_njets", returnError=True):
  h = ROOT.TH1D('h_tmp', 'h_tmp', 1,0,2)
  h.Sumw2()
  c.Draw("1>>h_tmp", "("+lumi+"*"+weight+")*("+cutString+")", 'goff')
  res = h.GetBinContent(1)
  resErr = h.GetBinError(1)
  del h
  if returnError:
    return res, resErr
  return res


def getVarValue(c, var, n=0):
  varNameHisto = var
  leaf = c.GetAlias(varNameHisto)
  if leaf!='':
    try:
      return c.GetLeaf(leaf).GetValue(n)
    except:
      raise Exception("Unsuccessful getVarValue for leaf %s and index %i"%(leaf, n))
  else:
    l = c.GetLeaf(var)
    if l:return l.GetValue(n)
    return float('nan')


def getObjDict(c, prefix, variables, i=0):
  variable = {}
  for var in variables:
    variable.update({var:c.GetLeaf(prefix+var).GetValue(i)})
  return variable
#  return var: c.GetLeaf(prefix+var).GetValue(i) for var in variables


def getJets(c):
  #addJetVars =  ['e', 'mv2c10', 'loosebad', 'tightbad', 'Jvt', 'truthLabel', 'HadronConeExclTruthLabelID', 'killedByPhoton']
  addJetVars =  ['m', 'bweight']
  if c=="branches":return ['n_jet','jet_pt','jet_eta', 'jet_phi'] + ['jet_'+x for x in addJetVars]
  nJet = int(getVarValue(c, 'n_jet'))
  jets=[]
  for i in range(nJet):
    jet = getObjDict(c, 'jet_', ['pt','eta', 'phi'], i)
    jet.update(getObjDict(c, 'jet_', addJetVars, i))
    jets.append(jet)
  return jets

def getLp(c):
  Lp = -10
  if c.GetLeaf('n_lep').GetValue()<1:
    return Lp
  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)
  metPt = c.GetLeaf('met').GetValue()
  metPhi = c.GetLeaf('met_phi').GetValue()
  
  Lp = (lepPt/sqrt(lepPt**2+metPt**2+2*lepPt*metPt*cos(lepPhi-metPhi)))*((lepPt+metPt*cos(lepPhi-metPhi))/sqrt(lepPt**2+metPt**2+2*lepPt*metPt*cos(lepPhi-metPhi)))
  return Lp


def mThree(c):
  m_three = 0.
  sumVec = 0.
  jet1 = ROOT.TLorentzVector()
  jet2 = ROOT.TLorentzVector()
  jet3 = ROOT.TLorentzVector()
  maxJ = ROOT.TLorentzVector()
  n_jet = int(getVarValue(c, 'n_jet'))
  jets = getJets(c)
  for i in xrange(n_jet):
    for j in xrange(i+1, n_jet):
      for k in xrange(j+1, n_jet):
        jet1.SetPtEtaPhiM(jets[i]['pt'], jets[i]['eta'], jets[i]['phi'], jets[i]['m'])
        jet2.SetPtEtaPhiM(jets[j]['pt'], jets[j]['eta'], jets[j]['phi'], jets[j]['m'])
        jet3.SetPtEtaPhiM(jets[k]['pt'], jets[k]['eta'], jets[k]['phi'], jets[k]['m'])
        temp = (jet1+jet2+jet3).Pt()
        if( temp > sumVec ):
          sumVec = temp
          maxJ = jet1 + jet2 + jet3
  m_three = maxJ.M()
  return m_three*0.001


def getDphiMetLep(c):
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)
  metPhi = c.GetLeaf('met_phi').GetValue()
  return deltaPhi(metPhi, lepPhi)


def getHt(c):
  n_jet = int(getVarValue(c, 'n_jet'))
  jets = getJets(c)
  ht = 0.
  for jet in jets:
    ht += jet['pt']
  return ht


def getMt(c):
  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)
  met = c.GetLeaf('met').GetValue()
  metPhi = c.GetLeaf('met_phi').GetValue()
  mt = sqrt( 2*lepPt*met*(1-cos(lepPhi - metPhi)) )
  return mt


def getAmt2(c):
  #Attention! Only works if minimal AnalysisBase release is set up together with https://gitlab.cern.ch/atlas-phys-susy-wg/CalcGenericMT2

  nJet = c.GetLeaf("n_jet").GetValue()  
  if (nJet < 2):
    return 0

  nLep = c.GetLeaf("n_lep").GetValue()
  if (nLep == 0):
    return 0
   
  jetindex1 = 0
  jetindex2 = 0
  max1 = -9999.
  
  jets = getJets(c)
  
  for j, jet in enumerate(jets):
    if jet['bweight'] > max1:
      max1 = jet['bweight']
      jetindex1 = j
     
  max2 = -9999.
  
  for j, jet in enumerate(jets):
    if (j == jetindex1): continue;
    if jet['bweight'] > max2:
      max2 = jet['bweight']
      jetindex2 = j
  
  jet1Btmp = ROOT.TLorentzVector()
  jet2Btmp = ROOT.TLorentzVector()
  jet1Btmp.SetPtEtaPhiM(jets[jetindex1]['pt'], jets[jetindex1]['eta'], jets[jetindex1]['phi'], jets[jetindex1]['m'])
  jet2Btmp.SetPtEtaPhiM(jets[jetindex2]['pt'], jets[jetindex2]['eta'], jets[jetindex2]['phi'], jets[jetindex2]['m'])


  jet1B = ROOT.TLorentzVector()
  jet2B = ROOT.TLorentzVector()
  jet1B.SetPtEtaPhiE(jet1Btmp.Pt()/1000.,jet1Btmp.Eta(),jet1Btmp.Phi(),jet1Btmp.E()/1000.);
  jet2B.SetPtEtaPhiE(jet2Btmp.Pt()/1000.,jet2Btmp.Eta(),jet2Btmp.Phi(),jet2Btmp.E()/1000.);
    
  leptontmp = ROOT.TLorentzVector()
  #ATTENTION! lepton flavour not provided, randomly decide whether lepton is muon or electron
  flavour = random.randint(0,1)
  if flavour:
    leptonmass = 0.510998910 #MeV
  else:
    leptonmass = 105.658367 #MeV

  lepton = ROOT.TLorentzVector()
  lepPt = c.GetLeaf("lep_pt").GetValue(0)
  lepEta = c.GetLeaf("lep_eta").GetValue(0)
  lepPhi = c.GetLeaf("lep_phi").GetValue(0)
  lepton.SetPtEtaPhiM(lepPt/1000., lepEta, lepPhi, leptonmass/1000.);
  
  MET = ROOT.TLorentzVector()
  met = c.GetLeaf("met").GetValue()
  metPhi = c.GetLeaf("met_phi").GetValue()
  MET.SetPtEtaPhiM(met/1000., 0., metPhi, 0.);
 
  mt2aCalc = ROOT.ComputeMT2(jet1B+lepton,jet2B, MET, 0., 80.)
  mt2a = mt2aCalc.Compute()

  mt2bCalc = ROOT.ComputeaMT2(jet2B+lepton, jet1B, MET, 0., 80.);
  mt2b = mt2bCalc.Compute()

  aMT2 = min(mt2a,mt2b)
  return aMT2


def getMetSig(c):
  met = c.GetLeaf("met").GetValue()
  ht = getHt(c)
  met_sig = met/sqrt(ht)
  return met_sig
  

def dphi_jet12(c):
    jet1Phi = c.GetLeaf('jet_phi').GetValue(0)
    jet2Phi = c.GetLeaf('jet_phi').GetValue(1)
    dphi = deltaPhi(jet1Phi, jet2Phi)
    return dphi


def dphi_lep_jet1(c):
    lepPhi = c.GetLeaf('lep_phi').GetValue(0)
    jet1Phi = c.GetLeaf('jet_phi').GetValue(0)
    dphi = deltaPhi(lepPhi, jet1Phi)
    return dphi

def met_over_squareroot_lepPt_softjetPt(c):
    met = c.GetLeaf('met').GetValue() *0.001
    njet = c.GetLeaf('n_jet').GetValue()
    lepPt = c.GetLeaf('lep_pt').GetValue(0) *0.001
    ht = 0.
    for i in range(1, int(njet)):
        jetpt = c.GetLeaf('jet_pt').GetValue(i) *0.001 
        ht += jetpt 
    ht += lepPt
    return met/(sqrt(ht))

def met_over_lepPt_softjetPt(c):
    met = c.GetLeaf('met').GetValue() *0.001
    njet = c.GetLeaf('n_jet').GetValue()
    lepPt = c.GetLeaf('lep_pt').GetValue(0) *0.001
    ht = 0.
    for i in range(1, int(njet)):
        jetpt = c.GetLeaf('jet_pt').GetValue(i) *0.001 
        ht += jetpt 
    ht += lepPt
    return met/ht

def CT1(c):                                                  
    met = c.GetLeaf('met').GetValue() *0.001
    ht = c.GetLeaf('ht').GetValue() *0.001
    CT1 = 0.
    if met < (ht-100):
        CT1 = met
    if met > (ht-100):
        CT1 = (ht -100)
    return CT1
    
    
def CT2(c):                                                  
    met = c.GetLeaf('met').GetValue() *0.001
    jet1Pt = c.GetLeaf('jet_pt').GetValue(0) *0.001
    CT2 = 0.
    if met < (jet1Pt-25):
        CT2 = met
    if met > (jet1Pt-25):
        CT2 = (jet1Pt -25)
    return CT2

def squareroot_lepPt_softjetPt_over_met(c):                  
    met = c.GetLeaf('met').GetValue() *0.001
    njet = c.GetLeaf('n_jet').GetValue()
    lepPt = c.GetLeaf('lep_pt').GetValue(0) *0.001
    ht = 0.
    for i in range(1, int(njet)):
        jetpt = c.GetLeaf('jet_pt').GetValue(i) *0.001 
        ht += jetpt 
    ht += lepPt
    return (sqrt(ht))/met


def getMbl(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)

  lep = ROOT.TLorentzVector()
  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepEta = c.GetLeaf('lep_eta').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)
  #ATTENTION! lepton flavour not provided, randomly decide whether lepton is muon or electron
  flavour = random.randint(0,1)
  if flavour:
    leptonmass = 0.510998910 #MeV
  else:
    leptonmass = 105.658367 #MeV

  lep.SetPtEtaPhiM(lepPt, lepEta, lepPhi, leptonmass)
 
  m_bl = -1. 
  if len(bjets)>0:
    bjet1 = ROOT.TLorentzVector()
    bjet1.SetPtEtaPhiM(bjets[0]['pt'], bjets[0]['eta'], bjets[0]['phi'], bjets[0]['m'])
    m_b1l = (bjet1 + lep).M()
    
    if len(bjets)>1:
      bjet2 = ROOT.TLorentzVector()
      bjet2.SetPtEtaPhiM(bjets[1]['pt'], bjets[1]['eta'], bjets[1]['phi'], bjets[1]['m'])
      m_b2l = (bjet2 + lep).M()
      m_bl = min(m_b1l, m_b2l)
    else:
      m_bl = m_b1l
  #else:
  #  print "WARNING! No bjet found!"

  return m_bl
  
def dr_b_l(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)

  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepEta = c.GetLeaf('lep_eta').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)

  lep = {'pt':lepPt, 'eta':lepEta, 'phi':lepPhi}

  dr = -1.
  if len(bjets)>0:
    dr = deltaR(bjets[0], lep)
  #else:
  #  print "WARNING! No bjet found!"
  return dr

  
def dr_j_l(c):
  jets = getJets(c)

  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepEta = c.GetLeaf('lep_eta').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)

  lep = {'pt':lepPt, 'eta':lepEta, 'phi':lepPhi}

  return deltaR(jets[0], lep)
  

def dphi_b_lep(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)

  lepPhi = c.GetLeaf('lep_phi').GetValue(0)

  dphi = -1.
  if len(bjets)>0:
    dphi =  deltaPhi(bjets[0]['phi'], lepPhi)
  #else:
  #  print "WARNING! No bjet found!"
  return dphi 


def dphi_b_met(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)

  metPhi = c.GetLeaf('met_phi').GetValue()

  dphi = -1.
  if len(bjets)>0:
    dphi =  deltaPhi(bjets[0]['phi'], metPhi)
  #else:
  #  print "WARNING! No bjet found!"
  return dphi


def mt_blMET(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)

  met_p4 = ROOT.TLorentzVector()
  met = c.GetLeaf("met").GetValue()
  met_phi = c.GetLeaf("met_phi").GetValue()
  met_p4.SetPtEtaPhiM(met, 0., met_phi, 0.)
               
  lep = ROOT.TLorentzVector()
  lepPt = c.GetLeaf('lep_pt').GetValue(0)
  lepEta = c.GetLeaf('lep_eta').GetValue(0)
  lepPhi = c.GetLeaf('lep_phi').GetValue(0)
  #ATTENTION! lepton flavour not provided, randomly decide whether lepton is muon or electron
  flavour = random.randint(0,1)
  if flavour:
    leptonmass = 0.510998910 #MeV
  else:
    leptonmass = 105.658367 #MeV

  lep.SetPtEtaPhiM(lepPt, lepEta, lepPhi, leptonmass)

  mT_blMET = -1.
  if len(bjets)>0:
    bjet1 = ROOT.TLorentzVector()
    bjet1.SetPtEtaPhiM(bjets[0]['pt'], bjets[0]['eta'], bjets[0]['phi'], bjets[0]['m'])
    mT_b1lMET = (bjet1 + lep + met_p4).Mt()
    
    if len(bjets)>1:
      bjet2 = ROOT.TLorentzVector()
      bjet2.SetPtEtaPhiM(bjets[1]['pt'], bjets[1]['eta'], bjets[1]['phi'], bjets[1]['m'])
      mT_b2lMET = (bjet2 + lep + met_p4).Mt()
      if (abs(mT_b1lMET - 173000.) < abs(mT_b2lMET - 173000.)):
        mT_blMET = mT_b1lMET
      else:
        mT_blMET = mT_b2lMET
    else:
        mT_blMET = mT_b1lMET
  #else:
  #  print "WARNING! No bjet found!"

  return mT_blMET

def getNbjet(c):
  jets = getJets(c)
  bjets = []
  for jet in jets:
    if (jet['bweight'] > 0.4803):
      bjets.append(jet)
  return len(bjets)

#!/usr/bin/env python

import ROOT
import os
import sys
import subprocess
import pandas as pd
import h5py
from root2pandas import root2pandas

CUT = "(n_bjet>0)"

variables = [
              "n_jet", "jet_pt", "jet_eta", "jet_phi", "jet_m", "jet_bweight",
              "n_bjet",
              "n_lep", "lep_pt", "lep_eta", "lep_phi",
              "met", "met_phi", "mt",
              "event_weight", "dsid"
]

def filesize(num):
  for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
    if abs(num) < 1024.0:
      return "%3.1f%sB" % (num, unit)
    num /= 1024.0
  return "%.1fYiB" % num

def main():
  global CUT
  if not len(sys.argv) == 3:
    print "Usage: cut-tree.py <source directory> <destination directory>"
    return
  elif len(sys.argv) == 4:
    src, dest, CUT = sys.argv[1:]
  else:
    src, dest = sys.argv[1:]

  if not os.path.isdir(src):
    print "No such folder '{}'".format(src)
    return

  if not os.path.isdir(dest):
    print "No such folder '{}'. Will create it now...".format(dest)
    os.makedirs(dest)

  # get all .root files in all subdirectories of <src>
  inFiles = [os.path.relpath(os.path.join(d, f), src) for (d, _, files) in os.walk(src) for f in files if f.endswith("root")] 

  print "Going to preprocess following files\nfrom\n\t{src}\nto\n\t{dest}\n(without overwriting existing files)\n\n- {files}" \
    .format(src=src, dest=dest, files="\n- ".join(inFiles))

  #while True:
  #  i = raw_input("Are you okay with that? (y|n) ").strip().lower()
  #  if i == "y":
  #    break
  #  elif i == "n":
  #    return

  for infile in inFiles:
    fSrc, fDest = os.path.join(src, infile), os.path.join(dest, infile)

    if not os.path.exists(os.path.dirname(fDest)):
      os.makedirs(os.path.dirname(fDest))

    if os.path.exists(fDest):
      print "Skipping " + infile
      continue

    #print "Copying '{}'...".format(f),

    f = ROOT.TFile(fSrc)

    # Get all trees in this file
    for name in set([k.GetName() for k in f.GetListOfKeys() if k.GetClassName() == "TTree"]):
      print "\nDEBUG: Processing " + name
      t = f.Get(name)

      # Resetting all branches and only set the variables from list variables
      t.SetBranchStatus("*", 0)
      for var in variables:
        t.SetBranchStatus(var, 1)

      t.Draw(">>eList",CUT) #Get the event list 'eList' which has all the events satisfying the cut
      elist = ROOT.gDirectory.Get("eList")
      nevents = elist.GetN()
      print "\nDEBUG: Will loop over {} events in file {} ".format(nevents, fSrc)
      first = True
      last = nevents-1
      for i in range(nevents):
        if i%100000==0:
          print i
        if first:
          fDest2 =fDest.replace(".root","_"+str(i/100000)+".root")
          fCopy = ROOT.TFile(fDest2, "RECREATE")
          fCopy.cd()
          tCopy = t.CloneTree(0)
          first = False
        if i%100000==0 and i!=0:
          fCopy.Write()
          fCopy.Close()
          fDest2 =fDest.replace(".root","_"+str(i/100000)+".root")
          fCopy = ROOT.TFile(fDest2, "RECREATE")
          fCopy.cd()
          tCopy = t.CloneTree(0)
        t.GetEntry(i)
        tCopy.Fill()
        if i == last:
          print "File {} processed!".format(fSrc)
          fCopy.Write()
          #fCopy.Close()
      # Open destination file for this tree. This is important as otherwise the tree would get written to
      # memory by default when doing CopyTree
      #fCopy = ROOT.TFile(fDest, "RECREATE")
      #fCopy.cd()

      #tCopy = t.CopyTree(CUT)
      #tCopy.AutoSave()
      #fCopy.Close()
      
    f.Close()
  # Create data frame from preprocessed .root file
  output = os.listdir(dest)
  for ofile in output:
    fRoot = os.path.join(dest, ofile) 

    f = ROOT.TFile(fRoot)

    # Get all trees in this file
    for name in set([k.GetName() for k in f.GetListOfKeys() if k.GetClassName() == "TTree"]):
      print "\nDEBUG: Processing " + name
      t = f.Get(name)
      outFile = fRoot.replace(".root", ".h5")
      df = root2pandas(fRoot, name)

      # save a pandas df to hdf5 (better to first convert it back to ndarray, to be fair)
      store = pd.HDFStore(outFile, "w")
      store.put(name, df, data_columns=df.columns)
      store.close()

      # let's load it back in to make sure it actually worked!
      new_df = pd.read_hdf(outFile, name)
      # -- check the shape again -- nice check to run every time you create a df
      print "File check! {}".format(os.path.join(dest, outFile))
      print "(Number of events, Number of branches): ",new_df.shape
    #print "OK Saved {}".format(filesize(os.stat(fSrc).st_size - os.stat(fDest).st_size))

  writeInfo = os.path.join(dest, "info.txt")
  #if os.path.exists(writeInfo):
  #  while True:
  #    i = raw_input("'{}' exists. Should it be overwritten? (y|n) ".format(writeInfo)).strip().lower()
  #    if i == "y":
  #      break
  #    elif i == "n":
  #      return

  with open(writeInfo, "w") as f:
    f.write("Preprocess Info!\nCut: {}".format(CUT))
    f.write("\nVariables:")
    for var in variables:
      f.write("\n"+var)

  subprocess.call('rm '+dest+'/*.root',shell=True)
  
if __name__ == "__main__":
  main()



preselection = [ 
                {'name':'n_jet',  'threshold':4,      'type':'geq'},
                {'name':'n_bjet',  'threshold':1,      'type':'geq'},
                {'name':'met',    'threshold':230e3,  'type':'geq'},
                {'name':'mt',    'threshold':100e3,  'type':'geq'},
                {'name':'n_lep',  'threshold':1,      'type':'exact'}
]

lumi = 1. 

nvar = [
        'met',
        'met_phi',
        'n_jet',
        'lep_pt[0]',
        'lep_eta[0]',
        'lep_phi[0]',
        'jet_pt[0]',
        'jet_eta[0]',
        'jet_phi[0]',
        'jet_m[0]',
        'jet_bweight[0]',
        'jet_pt[1]',
        'jet_eta[1]',
        'jet_phi[1]',
        'jet_m[1]',
        'jet_bweight[1]',
        'jet_pt[2]',
        'jet_eta[2]',
        'jet_phi[2]',
        'jet_m[2]',
        'jet_bweight[2]',
        'jet_pt[3]',
        'jet_eta[3]',
        'jet_phi[3]',
        'jet_m[3]',
        'jet_bweight[3]'
]

weight = [
          'event_weight',
]

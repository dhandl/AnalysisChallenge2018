import os
from collections import namedtuple

input = os.path.join(os.getenv('SampleDir'), 'skimmed/')

# define your samples here
Signal = [
  {'name':'stop_tN_500_327', 'path':input+'stop_tN_500_327/'},
  #{'name':'stop_tN_800_500', 'path':input+'stop_tN_800_500/'}

Background = [
  {'name':'bkgs',  'path':input+'bkgs/'}
]

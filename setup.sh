#!/bin/bash

ORIG_PATH=$PATH 

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# workaround for git-atlas bug
export git_config="git config"

lsetup "git --quiet"

export WorkDir=`pwd`
export SampleDir=$WorkDir/samples
export MLDir=$WorkDir/MLkit
export PYTHONPATH=$PYTHONPATH:$MLDir/python
export PYTHONPATH=$PYTHONPATH:$MLDir/config
export PATH=$PATH:$MLDir/scripts

# reappend the old PATH, sometimes this is messed up
export PATH=$PATH:$ORIG_PATH

echo "Setting up ML environment on lxplus..."
export PATH=/afs/cern.ch/work/d/dhandl/public/miniconda3/bin:$PATH
CondaDir="/afs/cern.ch/work/d/dhandl/public/miniconda3"
source activate testenv
source "$CondaDir/envs/testenv/bin/thisroot.sh"
export MKL_THREADING_LAYER=GNU
export KERAS_BACKEND=theano
python -c "from keras import backend"

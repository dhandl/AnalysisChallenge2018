# Analysis Challenge 2018

If you are only interested in the final output.root files feel free and have a look at **tN_diag_output_sig.root, tN_diag_output_bkg.root, tN_high_output_sig.root, tN_high_output_bkg.root, tN_diag_output_sig_multibin.root, tN_diag_output_bkg_multibin.root, tN_high_output_sig_multibin.root, tN_high_output_bkg_multibin.root**

If you only want to use the official stop1l samples, provided by the conveners, you can skip the preprocessing section. You will find the transformed .h5 files in *samples/stop1l/hdf5*.

If you only want to evaluate benchmark models with the existing classifiers you can leave out the Training section and jump to the Evaluate section. Depending in the samples you want to evaluate, you probably need to preprocess them according to the Preprocessing section.

The two existing classifiers I had trained and optimized are located in *MLkit/TrainedModels/models/* and are called *tN_diag.h5* and *tN_high.h5* targeting the two different benchmark models in the stop1l pahse space.
 
### Getting started 

Check your global git config before:

```
git config --list
```

Set up your config properly:

```
git config --global user.name "Firstname Lastname"
git config --global user.email example@example.com
```

Clone the repository and setup the enviromnent (additional info about the enviroment can be found below).

```
git clone ssh://git@gitlab.cern.ch:7999/dhandl/AnalysisChallenge2018.git
cd AnalysisChallenge2018
source setup.sh
```

### Preprocessing

The original .root files have to be preprocessed in a few steps. Therefore it is recommended to copy the original files in the *samples/stop1l/* directory.

First, we will add the b-jet multiplicity and the mt variable as a branch to the original files by executing:

```
addBranchToFile.py samples/stop1l/
```

Afterwards we need to transform the .root files into .h5 dataformat to either run the training or evaluate the trained model on an independent sample. (a cut on the b-jet multiplicity is set in the script!)

```
prepareSample.py samples/stop1l samples/stop1l/hdf5
```

Now you will find the corresponding .h5 files in the directory *samples/stop1l/hdf5* and an info.txt file that describes the cut and the variables that have been applied and transformed.


### Training ML algorithm

There are 3 configuration files located in *MLkit/config* where you should define the properties of your algorithm.

In *MLkit/config/variables.py* you can set up the preselection and variables used in the training, the luminosity and the weight.
In *MLkit/config/samples.py* you have to define the filepaths of your samples and in *MLkit/config/algorithm.py* you can define the hyperparameter of you learning algorithm.

Afterwards you can start the training:
```
trainModel.py -a <ALGORITHM> -d <DATASET_NAME> -m <MULTICLASSIFICATION> -n <OUTPUT_NAME> -o <OUTPUT_DIRECOTRY> -t <FRACTION_TRAININGEVENTS>
```

If you don't specify any output directory the dataset used for training and testing will be stored in *MLkit/TrainedModels/datasets/*, the model weights will be stored in *MLkit/TrainedModels/weights/* and the final model will be stored in *MLkit/TrainedModels/models/* .
In addition, a .pkl file called *OUTPUT_NAME_scaler.pkl* is stored in *MLkit/TrainedModels/models/* and will be used for data standardization when evaluating independent samples. You will also find a *OUTPUT_NAME_infofile.txt* file in *MLkit/TrainedModels/models/* where useful information about the algorithms and its hyperparameters, the samples, variables and preselecitons is stored.

### Evaluate Model

The evaluation of the trained algorithm on an independent set of input samples can be done with *MLkits/scripts/evaluate_signal.py*:

```
evaluate_signal.py -n <MODEL_NAME> - -s <SIGNAL_NAME>
```

Where MODEL_NAME is the name of your trained model located at *MLkit/TrainedModels/models/* and SIGNAL_NAME represents the prefix of your signal sample to be evaluated (i.e. stop_tN_800_500 for one of the officially provided benchmark model). The script determines a cut on the output score of the classifier, based on the expected significance and writes the number of remaining signal events and background events into two root files called MODEL_NAME_output_sig.root and MODEL_NAME_output_bkg.root, which can be used in a HistFitter configuration.

In addition, there are also two root files for a multi-binned scenario created. (ATTENTION! I did not evaluate the number of the bins used for the shape fit very much, I am only writing out the last five bins of the output score!)

The two existing classifiers I had trained and optimized are located in *MLkit/TrainedModels/models/* and are called *tN_diag.h5* and *tN_high.h5* targeting the two different benchmark models in the stop1l pahse space.

ATTENTION! If you run the evaluation script and use the two classifiers in the dedicated directory theoutput root files will be overwritten!

### Miscellanea

For general remarks, questions or suggestions please contact me: 
**david.handl@cern.ch**

Necessary python modules for machine learning applications are loaded via an Anaconda based environment. The enviroment is located here:

'''
/afs/cern.ch/work/d/dhandl/public/miniconda3
'''

Find a list of installed packages here: root, python, mkl, jupyter, numpy, scipy, matplotlib, scikit-learn, h5py, rootpy, root-numpy, pandas, scikit-image, seaborn, mkl-service, tqdm, binutils 
